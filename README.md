White Rabbit Timing Receiver in PMC format
=

How to check out and synthesize the PMC gate ware
==

On Linux:

<pre>
mkdir pmc
cd pmc/
git clone https://github.com/stefanrauch/bel_projects.git --recursive
cd bel_projects/
git checkout pmc
git submodule init
git submodule update
git submodule update --recursive

Optional: make pmc
Optional: Open project file bel_projects/syn/gsi_pmc/control/pci_pmc.qpf
with Quartus
</pre>

Be aware that you need a Quartus 64-bit version to synthesize the design.
